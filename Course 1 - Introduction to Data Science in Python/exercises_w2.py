import pandas as pd

purchase_1 = pd.Series({'Name': 'Chris',
                        'Item Purchased': 'Dog Food',
                        'Cost': 22.50})
purchase_2 = pd.Series({'Name': 'Kevyn',
                        'Item Purchased': 'Kitty Litter',
                        'Cost': 2.50})
purchase_3 = pd.Series({'Name': 'Vinod',
                        'Item Purchased': 'Bird Seed',
                        'Cost': 5.00})

df = pd.DataFrame([purchase_1, purchase_2, purchase_3], index=['Store 1', 'Store 1', 'Store 2'])



"""
For the purchase records from the pet store, how would you get a list of all 
items which had been purchased (regardless of where they might have been 
purchased, or by whom)?
"""
print(df['Item Purchased'])


"""
Write a query to return all of the names of people who bought products worth more than $3.00.
"""
print(df.where(df['Cost'] > 3.00)['Name'].dropna())
# or
print(df[df['Cost'] > 3.00]['Name'])



"""
Reindex the purchase records DataFrame to be indexed hierarchically,
first by store, then by person. Name these indexes 'Location' and 'Name'. 
Then add a new entry to it with the value of:

Name: 'Kevyn', Item Purchased: 'Kitty Food', Cost: 3.00 Location: 'Store 2'.
"""
df['Location'] = df.index
df = df.set_index('Location')
df = df.reset_index()
df = df.set_index(['Location', 'Name'])
print(df.index)

new_purchase = pd.Series(
    {
        'Item Purchased': 'Kitty Food',
        'Cost': 3.00
    },
    name=('Store 2', 'Kevyn')
)

df = df.append(new_purchase)
print(df)

# # Answer given:
# df = df.set_index([df.index, 'Name'])
# df.index.names = ['Location', 'Name']
# df = df.append(pd.Series(data={'Cost': 3.00, 'Item Purchased': 'Kitty Food'}, name=('Store 2', 'Kevyn')))
# print(df)
