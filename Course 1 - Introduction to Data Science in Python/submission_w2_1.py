"""
Part 1¶
The following code loads the olympics dataset (olympics.csv), 
which was derrived from the Wikipedia entry on All Time Olympic Games Medals, 
and does some basic data cleaning.

The columns are organized as # of Summer games, Summer medals, # of Winter games, 
Winter medals, total # number of games, total # of medals. Use this dataset to 
answer the questions below.
"""
import os
import pandas as pd


with open(os.path.dirname(__file__) + '/data/olympics.csv', 'r') as fh:
    df = pd.read_csv(fh, index_col=0, skiprows=1)

for col in df.columns:
    if col[:2]=='01':
        df.rename(columns={col:'Gold'+col[4:]}, inplace=True)
    if col[:2]=='02':
        df.rename(columns={col:'Silver'+col[4:]}, inplace=True)
    if col[:2]=='03':
        df.rename(columns={col:'Bronze'+col[4:]}, inplace=True)
    if col[:1]=='№':
        df.rename(columns={col:'#'+col[1:]}, inplace=True)

names_ids = df.index.str.split('\s\(') # split the index by '('

df.index = names_ids.str[0] # the [0] element is the country name (new index) 
df['ID'] = names_ids.str[1].str[:3] # the [1] element is the abbreviation or ID (take first 3 characters from that)

df = df.drop('Totals')



"""
Question 1
Which country has won the most gold medals in summer games?

This function should return a single string value.
"""
def answer_one():
    new_df = df['Gold']

    " Answer version 1 (lengthy) "
    # country = ''
    # max_gold = 0
    # for rec in new_df.index:
    #     if int(new_df[rec]) > max_gold:
    #         max_gold = int(new_df[rec])
    #         country = rec
    # print(country)
    
    " Answer version 2 (Shortened) "
    # max_gold = max([int(new_df[rec]) for rec in new_df.index])
    # print((df.index[df['Gold'] == max_gold].tolist()[0]))

    " Condensed Answer "
    return (df.index[df['Gold'] == max([int(new_df[rec]) for rec in new_df.index])].tolist()[0])



"""
Question 2
Which country had the biggest difference between their 
summer and winter gold medal counts?

This function should return a single string value.
"""
def answer_two():
    new_df = df[['Gold','Gold.1']]
    # new_df['Diff'] = new_df['Gold'] - new_df['Gold.1']

    " Answer version 1 (Shortened) "
    max_diff = 0
    country = ''
    for x in range(len(new_df)):
        if max_diff < abs(new_df['Gold'][x] - new_df['Gold.1'][x]):
            max_diff = abs(new_df['Gold'][x] - new_df['Gold.1'][x])
            country = new_df.index[x]
    return country

    " Condensed Answer "
    # return new_df['Diff'].idxmax()


 
"""
Question 3
Which country has the biggest difference between their summer gold medal 
counts and winter gold medal counts relative to their total gold medal count?

Summer Gold−Winter GoldTotal Gold
Summer Gold−Winter GoldTotal Gold
 
Only include countries that have won at least 1 gold in both summer and winter.

This function should return a single string value.
"""
def answer_three():

    new_df = df[(df['Gold'] >= 1) & (df['Gold.1'] >= 1)]  # Filter atleast 1 gold
    new_df = new_df[['Gold','Gold.1']]  # Clean Data
    new_df['Total Gold'] = new_df['Gold'] + new_df['Gold.1']
    new_df['Diff Relative'] = (new_df['Gold'] - new_df['Gold.1']) / new_df['Total Gold']

    return new_df['Diff Relative'].idxmax()



"""
Question 4
Write a function that creates a Series called "Points" which is a weighted value
where each gold medal (Gold.2) counts for 3 points, silver medals (Silver.2) 
for 2 points, and bronze medals (Bronze.2) for 1 point. The function should 
return only the column (a Series object) which you created, with the country 
names as indices.

This function should return a Series named Points of length 146
"""
def answer_four():
    new_df = df[['Gold.2','Silver.2','Bronze.2']]
    new_df['Points'] = (new_df['Gold.2'] * 3) + (new_df['Silver.2'] * 2) + (new_df['Bronze.2'])
    return pd.Series(new_df['Points'])



"""
Part 2
For the next set of questions, we will be using census data from the 
United States Census Bureau. Counties are political and geographic subdivisions
of states in the United States. This dataset contains population data for 
counties and states in the US from 2010 to 2015. See this document for a 
description of the variable names.

The census dataset (census.csv) should be loaded as census_df. Answer questions
using this as appropriate.
"""
with open(os.path.dirname(__file__) + '/data/census.csv', 'r') as fh:
    census_df = pd.read_csv(fh)



"""
Question 5
Which state has the most counties in it? (hint: consider the sumlevel key 
carefully! You'll need this for future questions too...)

This function should return a single string value.
"""
def answer_five():
    census = census_df.set_index(['STNAME', 'CTYNAME'])
    cdf = census.groupby(level = 0).sum()

    return cdf['COUNTY'].idxmax()



"""
Question 6
Only looking at the three most populous counties for each state, 
what are the three most populous states (in order of highest population 
to lowest population)? Use CENSUS2010POP.

This function should return a list of string values.
"""
def answer_six():
    census_df2 = census_df[census_df['SUMLEV'] == 50]
    census = census_df2.sort_values(['STNAME','CENSUS2010POP'], ascending=False).groupby('STNAME').head(3)
    ans = census.groupby('STNAME').sum().sort_values(['CENSUS2010POP'], ascending=False).head(3).index.tolist()
    return ans



"""
Question 7
Which county has had the largest absolute change in population within the 
period 2010-2015? (Hint: population values are stored in columns 
POPESTIMATE2010 through POPESTIMATE2015, you need to consider all six columns.)

e.g. If County Population in the 5 year period is 100, 120, 80, 105, 100, 130, 
then its largest change in the period would be |130-80| = 50.

This function should return a single string value.
"""
def answer_seven():
    census_df2 = census_df[census_df['SUMLEV'] == 50]
    census = census_df2[
        [
            'CTYNAME',
            'POPESTIMATE2010',
            'POPESTIMATE2011',
            'POPESTIMATE2012',
            'POPESTIMATE2013',
            'POPESTIMATE2014',
            'POPESTIMATE2015'
        ]
    ].set_index('CTYNAME')
    census['largest_abs_change'] = census.max(axis=1) - census.min(axis=1)
    return census.sort_values('largest_abs_change', ascending=False).index[0]



"""
Question 8
In this datafile, the United States is broken up into four 
regions using the "REGION" column.

Create a query that finds the counties that belong to regions 1 or 2, 
whose name starts with 'Washington', and whose POPESTIMATE2015 was 
greater than their POPESTIMATE 2014.

This function should return a 5x2 DataFrame with the 
columns = ['STNAME', 'CTYNAME'] and the same index ID as the census_df 
(sorted ascending by index).
"""
def answer_eight():
    census_df2 = census_df[census_df['SUMLEV'] == 50]
    census_df2 = census_df2[
        ((census_df2['REGION'] == 1) | (census_df2['REGION'] == 2)) &
        (census_df2['CTYNAME'].str.startswith('Washington ')) &
        (census_df2['POPESTIMATE2015'] > census_df2['POPESTIMATE2014'])
    ]
    return census_df2[['STNAME', 'CTYNAME']]



# print(answer_one())
# print(answer_two())
# print(answer_three())
# print(answer_four())
# print(answer_five())
# print(answer_six())
# print(answer_seven())
# print(answer_eight())
